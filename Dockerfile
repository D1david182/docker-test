FROM centos:7
MAINTAINER Daniel
RUN yum -y update && \
    yum -y install httpd && \
    yum clean all
COPY ./call_redshift.sh /
RUN chmod +x /call_redshift.sh
ENTRYPOINT ["/call_redshift.sh"]
CMD ["Some random query string"]
